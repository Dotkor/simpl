## begin fhead
#
# funcname "uiwdemo"
# name "UI Widgets Demo"
# desc "Does not do much apart from showing supported UI widgets"
# arg "f_spinbox" spinbox int [1, inf] "Spinbox"
# arg "f_dropdown" dropdown ["Option 1", "Option 2"] "Dropdown"
# arg "f_checkbox" checkbox "Checkbox"
#
## end fhead


def uiwdemo(img, args):
    return img