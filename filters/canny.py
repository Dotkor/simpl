## begin fhead
#
# funcname "canny_edge"
# name "Canny edge detector"
# desc "Detecting edges"
# arg "a" spinbox int [0, inf] "Horizontal "
# arg "b" spinbox int [0, inf] "Vertical "
#
## end fhead

import cv2
import matplotlib.pyplot as plt
 
def canny_edge(img,args):
    a= args['a']
    b= args['b']
    tmp = cv2.Canny(img,a,b,L2gradient=False)

    return tmp

